//
//  SampleUITests.swift
//  SampleUITests
//
//  Created by 4Axis on 2022-03-07.
//

import XCTest

class SampleUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
   }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    func testExampleOne(){
        
        let app = XCUIApplication()
        app.launch()
        //sample
        let label = app.staticTexts["Sample Demo"]
        label.tap()
        
    }
    
    func testExampleTwo(){
          
          let app = XCUIApplication()
          app.launch()
          
          let submitBtn = app/*@START_MENU_TOKEN@*/.buttons["submitButton"]/*[[".buttons[\"Submit\"]",".buttons[\"submitButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
          submitBtn.tap()
          
      }
    
    func testExampleThree(){
        
        let app = XCUIApplication()
        app.launch()
        
        let submitBtn = app/*@START_MENU_TOKEN@*/.buttons["submitButton"]/*[[".buttons[\"Submit\"]",".buttons[\"submitButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        submitBtn.tap()
        
        let alert = app.alerts["Welcome to Demo"]
        XCTAssertTrue(alert.exists)
        
        let alertOkBtn = app.alerts["Welcome to Demo"].scrollViews.otherElements.buttons["OK"]
        alertOkBtn.tap()
        
    }
    
    func testExampleFour(){
          
          let app = XCUIApplication()
          app.launch()
          
          let submitBtn = app/*@START_MENU_TOKEN@*/.buttons["submitButton"]/*[[".buttons[\"Submit\"]",".buttons[\"submitButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
          submitBtn.tap()
          
      }

    
}
